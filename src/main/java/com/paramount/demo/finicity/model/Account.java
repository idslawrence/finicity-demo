package com.paramount.demo.finicity.model;

/**
 * Project: finicity Author: lli on 8/22/2019
 **/
public class Account {
    private String id;
    private String number;
    private String name;
    private double balance;
    private String type;
    private String status;
    private int aggregationStatus;
    private String customerId;
    private String institutionId;
    private long createdDate;
    private long lastUpdatedDate;
    private String currency;
    private int institutionLoginId;
    private int displayPosition;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }

    public long getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(long createdDate) {
        this.createdDate = createdDate;
    }

    public long getLastUpdatedDate() {
        return lastUpdatedDate;
    }

    public void setLastUpdatedDate(long lastUpdatedDate) {
        this.lastUpdatedDate = lastUpdatedDate;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public int getInstitutionLoginId() {
        return institutionLoginId;
    }

    public void setInstitutionLoginId(int institutionLoginId) {
        this.institutionLoginId = institutionLoginId;
    }

    public int getDisplayPosition() {
        return displayPosition;
    }

    public void setDisplayPosition(int displayPosition) {
        this.displayPosition = displayPosition;
    }

    public int getAggregationStatus() {
        return aggregationStatus;
    }

    public void setAggregationStatus(int aggregationStatus) {
        this.aggregationStatus = aggregationStatus;
    }
}
