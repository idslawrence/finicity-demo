package com.paramount.demo.finicity.model;

import java.util.Map;

/**
 * Project: finicity Author: lli on 8/21/2019
 **/
public class ConnectLinkRequest {
    private String partnerId;
    private String customerId;
    private String redirectUri;
    private String type;
    private String institutionId;
    private String webhook;
    private String webhookContentType;
    private Map<String, String> webhookData;

    public String getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getRedirectUri() {
        return redirectUri;
    }

    public void setRedirectUri(String redirectUri) {
        this.redirectUri = redirectUri;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }

    public String getWebhook() {
        return webhook;
    }

    public void setWebhook(String webhook) {
        this.webhook = webhook;
    }

    public String getWebhookContentType() {
        return webhookContentType;
    }

    public void setWebhookContentType(String webhookContentType) {
        this.webhookContentType = webhookContentType;
    }

    public Map<String, String> getWebhookData() {
        return webhookData;
    }

    public void setWebhookData(Map<String, String> webhookData) {
        this.webhookData = webhookData;
    }
}
