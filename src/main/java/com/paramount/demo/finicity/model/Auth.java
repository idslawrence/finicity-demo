package com.paramount.demo.finicity.model;

/**
 * Project: finicity Author: lli on 8/20/2019
 **/
public class Auth {
    private String partnerId;
    private String partnerSecret;

    public String getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }

    public String getPartnerSecret() {
        return partnerSecret;
    }

    public void setPartnerSecret(String partnerSecret) {
        this.partnerSecret = partnerSecret;
    }
}
