package com.paramount.demo.finicity.model;

import java.time.Instant;

/**
 * Project: finicity Author: lli on 8/19/2019
 **/
public class Token {
    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
