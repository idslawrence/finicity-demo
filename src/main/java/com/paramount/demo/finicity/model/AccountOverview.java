package com.paramount.demo.finicity.model;

import java.util.List;

/**
 * Project: finicity Author: lli on 8/22/2019
 **/
public class AccountOverview {
    private List<Account> accounts;

    public List<Account> getAccounts() {
        return accounts;
    }

    public void setAccounts(List<Account> accounts) {
        this.accounts = accounts;
    }
}
