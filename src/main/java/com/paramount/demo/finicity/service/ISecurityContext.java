package com.paramount.demo.finicity.service;

import com.paramount.demo.finicity.model.Token;

/**
 * Project: finicity Author: lli on 8/19/2019
 **/
public interface ISecurityContext {
    String getToken();

    void inValidate(String token);
}
