package com.paramount.demo.finicity.service;

import com.google.common.collect.ImmutableMap;
import com.paramount.demo.finicity.config.FinicityConfig;
import com.paramount.demo.finicity.model.Account;
import com.paramount.demo.finicity.model.AccountDetail;
import com.paramount.demo.finicity.model.AccountOverview;
import com.paramount.demo.finicity.model.ConnectLinkRequest;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.nutz.json.Json;
import org.nutz.mapl.Mapl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.RequestEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

/**
 * Project: finicity Author: lli on 8/19/2019
 **/
@Service
public class FinicityService implements IFinicityService {
    
    private static final Logger logger = LoggerFactory.getLogger(FinicityService.class);
    static final String APP_KEY = "Finicity-App-Key";
    static final String APP_TOKEN = "Finicity-App-Token";

    private static final int RETRY = 2;

    private FinicityConfig config;
    private RestTemplate template;
    private String baseUrl;
    private ISecurityContext securityContext;
    private String institution;
    private boolean forTest;

    public FinicityService(FinicityConfig cfg,
            RestTemplateBuilder templateBuilder,
            ISecurityContext securityContext,
            @Value("${institutionId}") String institution,
            @Value("${finicity.test.mode:true}") boolean forTest) {
        config = cfg;
        template = templateBuilder.build();
        baseUrl = config.getBaseUrl();
        if (baseUrl != null && !baseUrl.endsWith("/")) {
            baseUrl += "/";
        }
        this.securityContext = securityContext;
        this.institution = institution;
        this.forTest = forTest;
    }

    @Override
    public String addCustomer(String customer) {
        return execute(token -> {
            HttpHeaders headers = headers(token);
            String reqUrl = forTest ? "aggregation/v1/customers/testing" : "aggregation/v1/customers/active";
            RequestEntity req = new RequestEntity<>(ImmutableMap.of("username", customer), headers, HttpMethod.POST, URI.create(url(reqUrl)));
            try {
                Map<String, String> resp = template.exchange(req, new ParameterizedTypeReference<Map<String, String>>() {
                }).getBody();
                return resp == null ? null : resp.get("id");
            } catch (HttpStatusCodeException e) {
                Object error = Json.fromJson(e.getResponseBodyAsString());
                if (error != null && "110002".equals(Mapl.cell(error, "code").toString())) {
                    return null;
                }
                throw e;
            }
        });
    }

    public String getCustomerId(String customer) {
        return execute(token -> {
            HttpHeaders headers = headers(token);
            HttpEntity req = new HttpEntity(headers);
            UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url("aggregation/v1/customers"))
                    .queryParam("username", customer);
            Map resp = template.exchange(builder.toUriString(), HttpMethod.GET, req, Map.class).getBody();
            if (resp == null) {
                return null;
            }
            int cnt = (int)resp.get("found");
            for (int i = 0; i < cnt; i++) {
                String pos = "customers[" + i + "]";
                if (customer.equals(Mapl.cell(resp, pos + ".username"))) {
                    return (String)Mapl.cell(resp, pos + ".id");
                }
            }
            return null;
        });
    }

    @Override
    public String getConnectLink(String customerId, String app) {
        return execute(token -> {
            HttpHeaders headers = headers(token);
            ConnectLinkRequest link = new ConnectLinkRequest();
            link.setCustomerId(customerId);
            link.setPartnerId(config.getAuth().getPartnerId());
            link.setType(app);
            link.setInstitutionId(institution);
            link.setRedirectUri("http://localhost:8080/ecs/summary?session=9999&customerId=" + customerId);
            link.setWebhook("https://235592b2.ngrok.io/ecs/webhook");
            link.setWebhookContentType("application/json");
            link.setWebhookData(ImmutableMap.of("session", "9999"));
            RequestEntity req = new RequestEntity<>(link, headers, HttpMethod.POST, URI.create(url("connect/v1/generate")));
            Map resp = template.exchange(req, Map.class).getBody();
            return resp == null ? null : (String)resp.get("link");
        });
    }

    @Override
    public List<Account> getCustomerAccounts(String customerId) {
        AccountOverview overview = execute(token -> {
            HttpHeaders headers = headers(token);
            HttpEntity req = new HttpEntity(headers);
            return template.exchange(url("aggregation/v1/customers/{customerId}/institutions/{institutionId}/accounts"),
                    HttpMethod.GET, req, AccountOverview.class, customerId, institution).getBody();
        });
        if (overview != null && overview.getAccounts() != null) {
            List<Account> allAccounts = overview.getAccounts();
            List<Account> result = new ArrayList<>();
            List<Account> toDelete = new ArrayList<>();
            for (Account acct : allAccounts) {
                String type = acct.getType();
                if (type != null) {
                    type = type.toLowerCase();
                    if (type.contains("checking") || type.contains("saving")) {
                        result.add(acct);
                        continue;
                    }
                }
                toDelete.add(acct);
            }
            deleteCustomerAccounts(toDelete);
            return result;
        }
        return Collections.emptyList();
    }

    private void deleteCustomerAccounts(List<Account> accounts) {
        execute(token -> {
            HttpHeaders headers = headers(token);
            HttpEntity req = new HttpEntity(headers);
            accounts.parallelStream().forEach(account -> {
                logger.info("Deleting account {} for customer {}", account.getName(), account.getCustomerId());
                template.exchange(url("/aggregation/v1/customers/{customerId}/accounts/{accountId}"),
                        HttpMethod.DELETE, req, String.class, account.getCustomerId(), account.getId()).getBody();
                logger.info("Account [{}] was removed from customer {}", account.getName(), account.getCustomerId());
            });
            return null;
        });
    }

    @Override
    public List<Account> refreshCustomerAccounts(String customerId) {
        AccountOverview overview = execute(token -> {
            HttpHeaders headers = headers(token);
            HttpEntity req = new HttpEntity(headers);
            return template.exchange(url("aggregation/v1/customers/{customerId}/accounts"),
                    HttpMethod.POST, req, AccountOverview.class, customerId).getBody();
        });

        if (overview == null || overview.getAccounts() == null) {
            return Collections.emptyList();
        }

        return overview.getAccounts().stream()
                .filter(acct -> institution.equals(acct.getInstitutionId()))
                .collect(Collectors.toList());
    }

    @Override
    public AccountDetail getAccountDetail(String customerId, String accountId) {
        return execute(token -> {
            HttpHeaders headers = headers(token);
            HttpEntity req = new HttpEntity(headers);
//            Map info = doGetAccountDetail(url("aggregation/v1/customers/{customerId}/accounts/{accountId}"), customerId, accountId, req);
            AccountDetail result = new AccountDetail();
            Runnable getDetail = () -> {
                logger.info("Fetching ach detail for account {}", accountId);
                Map detail = doGetAccountDetail(url("aggregation/v1/customers/{customerId}/accounts/{accountId}/details"), customerId, accountId, req);
                logger.info("Ach detail was fetched for account {}", accountId);
                if (detail != null) {
                    result.setRouting((String)detail.get("routingNumber"));
                    result.setNumber((String)detail.get("realAccountNumber"));
                }
            };
            Runnable getOwner = () -> {
                logger.info("Fetching owner info for account {}", accountId);
                Map owner = doGetAccountDetail(url("aggregation/v1/customers/{customerId}/accounts/{accountId}/owner"),customerId, accountId, req);
                if (owner != null) {
                    result.setName((String)owner.get("ownerName"));
                    result.setAddress((String)owner.get("ownerAddress"));
                }
                logger.info("Owner info was fetched for account {}", accountId);
            };
            ExecutorService exec = Executors.newCachedThreadPool();
            Stream.of(exec.submit(getDetail), exec.submit(getOwner))
                    .forEach(f -> {
                        try {
                            f.get();
                        }catch (Throwable ignore){}
                    });
            exec.shutdown();
            return result;
        });
    }

    private Map doGetAccountDetail(String url, String customerId, String accountId, HttpEntity req) {
        try {
            return template.exchange(url, HttpMethod.GET, req, Map.class, customerId, accountId).getBody();
        } catch (RestClientException e) {
            e.printStackTrace();
            return null;
        }
    }

    private String urlEncode(String str) {
        try {
            return URLEncoder.encode(str, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            logger.warn("Failed to encode " + str, e);
            return str;
        }
    }

    private String url(String path) {
        return baseUrl + path;
    }

    private <T> T execute(Function<String, T> runner) {
        int retryCount = 0;
        do {
            String token = securityContext.getToken();
            try {
                return runner.apply(token);
            }catch (HttpStatusCodeException e) {
                logger.warn(e.getResponseBodyAsString());
                if (e.getStatusCode() == HttpStatus.UNAUTHORIZED) {
                    securityContext.inValidate(token);
                    logger.warn("Token {} expired, try again", token);
                    retryCount++;
                }else {
                    throw e;
                }
            }
        }while (retryCount < RETRY);
        throw new IllegalStateException("Failed to get valid token");
    }

    private HttpHeaders headers(String token) {
        HttpHeaders headers = new HttpHeaders();
        headers.add(APP_KEY, config.getKey());
        headers.add(APP_TOKEN, token);
        return headers;
    }
}
