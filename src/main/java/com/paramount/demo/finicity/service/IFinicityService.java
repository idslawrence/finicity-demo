package com.paramount.demo.finicity.service;

import com.paramount.demo.finicity.model.Account;
import com.paramount.demo.finicity.model.AccountDetail;
import java.util.List;

/**
 * Project: finicity Author: lli on 8/19/2019
 **/
public interface IFinicityService {
    /**
     * Add customer and return its id
     * @param customer name of the customer
     */
    String addCustomer(String customer);

    String getCustomerId(String customer);

    String getConnectLink(String customerId, String app);

    List<Account> getCustomerAccounts(String customerId);

    List<Account> refreshCustomerAccounts(String customerId);

    AccountDetail getAccountDetail(String customerId, String accountId);
}
