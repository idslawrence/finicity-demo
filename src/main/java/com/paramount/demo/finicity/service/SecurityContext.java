package com.paramount.demo.finicity.service;

import com.paramount.demo.finicity.config.FinicityConfig;
import com.paramount.demo.finicity.model.Token;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URI;
import org.nutz.json.Json;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.RequestEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

/**
 * Project: finicity Author: lli on 8/21/2019
 **/
@Component
public class SecurityContext implements ISecurityContext {

    private FinicityConfig cfg;
    private RestTemplate template;
    private String token;

    public SecurityContext(FinicityConfig cfg, RestTemplateBuilder templateBuilder) {
        this.cfg = cfg;
        template = templateBuilder.build();
        try {
            token = Json.fromJson(Token.class, new FileReader("./token.json")).getToken();
        }catch (Throwable ignore) {}
    }

    @Override
    public String getToken() {
        return doGetToken();
    }

    @Override
    public void inValidate(String token) {
        this.token = null;
    }


    private String doGetToken() {
        if (token == null) {
            HttpHeaders headers = new HttpHeaders();
            headers.add(FinicityService.APP_KEY, cfg.getKey());
            StringBuilder sbd = new StringBuilder(cfg.getBaseUrl());
            if (!cfg.getBaseUrl().endsWith("/")) {
                sbd.append("/");
            }
            sbd.append("aggregation/v2/partners/authentication");
            RequestEntity req = new RequestEntity<>(cfg.getAuth(), headers, HttpMethod.POST, URI.create(sbd.toString()));
            Token t = template.exchange(req, Token.class).getBody();
            if (t != null) {
                try {
                    Json.toJson(new FileWriter("./token.json"), t);
                } catch (IOException ignore) {
                }
            }
            token = t == null ? null : t.getToken();
        }
        return token;
    }
}
