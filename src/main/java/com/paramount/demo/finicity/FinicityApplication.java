package com.paramount.demo.finicity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FinicityApplication {

    public static void main(String[] args) {
        SpringApplication.run(FinicityApplication.class, args);
    }

}
