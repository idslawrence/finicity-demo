package com.paramount.demo.finicity.web;

import com.paramount.demo.finicity.service.ISecurityContext;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Project: finicity Author: lli on 8/19/2019
 **/
@RestController
public class FinicityController {
    private ISecurityContext securityContext;

    public FinicityController(ISecurityContext securityContext) {
        this.securityContext = securityContext;
    }

    @GetMapping("/token")
    public String getToken() {
        return securityContext.getToken();
    }
}
