package com.paramount.demo.finicity.web;

import com.paramount.demo.finicity.model.Account;
import com.paramount.demo.finicity.model.AccountDetail;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.web.util.UriComponents;

/**
 * Project: finicity Author: lli on 8/21/2019
 **/
@Controller
public class EcsController {

    private RestTemplate template;

    public EcsController(RestTemplateBuilder templateBuilder) {
        this.template = templateBuilder.build();
    }

    @GetMapping("/")
    public ModelAndView main() {
        return new ModelAndView("index");
    }

    @GetMapping("/auth")
    public ModelAndView auth() {
        return new ModelAndView("auth");
    }

    @GetMapping("/vach")
    public ModelAndView vach() {
        return new ModelAndView("vach");
    }

    @GetMapping("/summary")
    public ModelAndView summary(String customerId, HttpServletRequest req) {
        UriComponents url = ServletUriComponentsBuilder.fromServletMapping(req).path("/accounts")
                .queryParam("customerId", customerId)
                .build();
        List<Account> accounts = template.exchange(url.toUriString(), HttpMethod.GET, null, new ParameterizedTypeReference<List<Account>>() {
        }).getBody();
        ModelAndView mv = new ModelAndView("summary");
        mv.addObject("accounts", accounts);
        return mv;
    }

    @GetMapping("/detail")
    public ModelAndView detail(String customerId, String accountId, HttpServletRequest req) {
        UriComponents url = ServletUriComponentsBuilder.fromServletMapping(req).path("/accountdetail")
                .queryParam("customerId", customerId)
                .queryParam("accountId", accountId)
                .build();
        AccountDetail detail = template.exchange(url.toUriString(), HttpMethod.GET, null, AccountDetail.class).getBody();
        ModelAndView mv = new ModelAndView("detail");
        mv.addObject("detail", detail);
        return mv;
    }

    @RequestMapping("/webhook")
    @ResponseBody
    public void webhook(HttpServletRequest request) throws Exception{
        System.out.println(request.getReader().lines().collect(Collectors.joining("\n")));
    }
}
