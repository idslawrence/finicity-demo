package com.paramount.demo.finicity.web;

import com.paramount.demo.finicity.model.Account;
import com.paramount.demo.finicity.model.AccountDetail;
import com.paramount.demo.finicity.service.IFinicityService;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Project: finicity Author: lli on 8/21/2019
 **/
@RestController
public class BigController {

    private IFinicityService service;

    public BigController(IFinicityService service) {
        this.service = service;
    }

    @PostMapping("/accountsummary")
    public String accountSummary(String customer, HttpServletRequest req) {
        String customerId = service.addCustomer(customer);
        System.out.println("Customer id: " + customerId);
        if (customerId != null) {
            return service.getConnectLink(customerId, "lite");
        } else {
            customerId = service.getCustomerId(customer);
            List<Account> accounts = service.refreshCustomerAccounts(customerId);
            if (accounts.isEmpty()) {
                return service.getConnectLink(customerId, "lite");
            } else if (accounts.stream().anyMatch(acct -> acct.getAggregationStatus() != 0)) {
                return service.getConnectLink(customerId, "fix");
            } else {
//                return "summary?customerId=" + customerId;
                return service.getConnectLink(customerId, "lite");
            }
        }
    }

    @GetMapping("/accounts")
    public List<Account> getAccountList(String customerId) {
        return service.getCustomerAccounts(customerId);
    }

    @GetMapping("/accountdetail")
    public AccountDetail getAccountDetail(String customerId, String accountId) {
        System.out.println("Account id: " + accountId);
        return service.getAccountDetail(customerId, accountId);
    }
}
