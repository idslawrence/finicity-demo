package com.paramount.demo.finicity.config;

import com.paramount.demo.finicity.model.Auth;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * Project: finicity Author: lli on 8/19/2019
 **/
@ConfigurationProperties(prefix = "finicity")
@Configuration
public class FinicityConfig {
    private String baseUrl;
    private String key;
    private Auth auth;

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Auth getAuth() {
        return auth;
    }

    public void setAuth(Auth auth) {
        this.auth = auth;
    }
}
